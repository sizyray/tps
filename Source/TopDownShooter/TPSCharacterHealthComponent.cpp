// Fill out your copyright notice in the Description page of Project Settings.


#include "TPSCharacterHealthComponent.h"
#include "Kismet/GameplayStatics.h"

void UTPSCharacterHealthComponent::ChangeHealthValue(float ChangeValue)
{
	

	float CurrentDamage = ChangeValue * CoefDamage;
	if (Shield > 0.0f && ChangeValue < 0.0f)
	{
		ChangeShieldValue(ChangeValue);
		if (Shield <= 0.0f)
		{
			FVector LocationToSpawn = GetOwner()->GetActorLocation();
			UGameplayStatics::PlaySoundAtLocation(GetWorld(), SoundShieldBroken,LocationToSpawn);
			OnShieldBroken.Broadcast();
		}
	}
	else
	{
		Super::ChangeHealthValue(ChangeValue);
	}
}

float UTPSCharacterHealthComponent::GetCurrentShield()
{
	return Shield;
}

void UTPSCharacterHealthComponent::ChangeShieldValue(float ChangeValue)
{
	Shield += ChangeValue;
	if (Shield > 100.0f)
	{
		Shield = 100.0f;
	}
	else
	{
		if (Shield<0.0f)
		{
			Shield = 0.0f;
		}
	}
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_CollDownShieldTimer, this, &UTPSCharacterHealthComponent::CollDownShieldEnd, CollDownShieldRecoveryTime, false);
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
	}
	OnShieldChange.Broadcast(Shield, ChangeValue);
}

void UTPSCharacterHealthComponent::CollDownShieldEnd()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ShieldRecoveryRateTimer, this, &UTPSCharacterHealthComponent::RecoveryShield, ShiedRecoveryRate, true);
	}
}

void UTPSCharacterHealthComponent::RecoveryShield()
{
	float tmp = Shield;
	tmp = tmp + ShieldRecoveryValue;
	if (tmp > 100.0f)
	{
		Shield = 100.0f;
		if (GetWorld())
		{
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
		}
	}
	else
	{
		Shield = tmp;
	}
	OnShieldChange.Broadcast(Shield, ShieldRecoveryValue);
}
