// Fill out your copyright notice in the Description page of Project Settings.


#include "Types.h"
#include "TopDownShooter.h"
#include "TPS_IGameActor.h"


void UTypes::AddEffectBySurfaceType(AActor* TakeEffectActor , TSubclassOf<UTPS_StateEffect> AddEffectClass, EPhysicalSurface SurfaceType)
{
	if (SurfaceType != EPhysicalSurface::SurfaceType_Default&& TakeEffectActor&& AddEffectClass)
	{
		UTPS_StateEffect* myEffect = Cast<UTPS_StateEffect>(AddEffectClass->GetDefaultObject());
		if (myEffect)
		{
			bool bIsCanAdd = false;
			int8 i = 0;
			while (i < myEffect->PossibleInteractSurface.Num() && !bIsCanAdd)
			{
				if (myEffect->PossibleInteractSurface[i] == SurfaceType)
				{
					bIsCanAdd = true;
					bool bIsCanAddEffect = false;
					if (myEffect->bIsNotStacable)
					{
					
						int8 j = 0;
						TArray<UTPS_StateEffect*>CurrentEffect;
						ITPS_IGameActor* myInterface = Cast<ITPS_IGameActor>(TakeEffectActor);
						if (myInterface)
						{
							CurrentEffect = myInterface->GetAllCurrentEffects();

						}
						if(CurrentEffect.Num()>0)
						{
							while (j < CurrentEffect.Num() && !bIsCanAddEffect)
							{
								if (CurrentEffect[j]->GetClass() != AddEffectClass)
								{
									bIsCanAddEffect = true;
								}
								j++;
							}
						}
						else
						{
							bIsCanAddEffect = true;
						}

					}
					else
					{
						bIsCanAddEffect = true;
					}
					if (bIsCanAddEffect)
					{
						UTPS_StateEffect* NewEffect = NewObject<UTPS_StateEffect>(TakeEffectActor, AddEffectClass);
						if (NewEffect)
						{
							NewEffect->InitObject(TakeEffectActor);
						}
					}
					
				}
				i++;
			}
		}
		
	}
}
